package com.cunyhackathon2017.dds.listeners;

import com.cunyhackathon2017.dds.models.Employee;
import com.cunyhackathon2017.dds.models.Team;

import java.util.List;

/**
 * Created by Istolla on 4/30/2017.
 */
public interface TeamsReceivedListener {
    void onTeamsReceived(List<Team> teams);
}
