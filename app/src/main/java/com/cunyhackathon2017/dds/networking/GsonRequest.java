package com.cunyhackathon2017.dds.networking;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.JsonSyntaxException;
import com.cunyhackathon2017.dds.tools.JsonConverter;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * Created by Istolla on 4/29/2017.
 */
public class GsonRequest<T> extends Request<T> {
    private final Type inputType, outputType;
    private final Map<String, String> headers;
    private final Response.Listener<T> listener;
    private Object inputObject;
    private String mediaType;
    private static final String PROTOCOL_CHARSET = "utf-8";

    public GsonRequest(
            String url, Type outputType, Map<String, String> headers, Response.Listener<T> listener,
            Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.outputType = outputType;
        this.inputType = null;
        this.headers = headers;
        this.listener = listener;
    }

    public GsonRequest(
            int method, String url, Type outputType, Type inputType, Object inputObject,
            String mediaType, Map<String, String> headers, Response.Listener<T> listener,
            Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.outputType = outputType;
        this.inputType = inputType;
        this.inputObject = inputObject;
        this.mediaType = mediaType;
        this.headers = headers;
        this.listener = listener;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        String jsonString = JsonConverter.toJson(inputObject, inputType);
        byte[] result = null;
        try {
            result = jsonString.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public String getBodyContentType() {
        return "application/json; charset=utf-8";
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        Response<T> deliverResponse = null;
        try {
            String json = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            T parseObject = JsonConverter.fromJson(json, outputType);
            deliverResponse = Response
                    .success(parseObject, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException | JsonSyntaxException e) {
            deliverResponse = Response.error(new ParseError(e));
        }
        return deliverResponse;
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }
}
