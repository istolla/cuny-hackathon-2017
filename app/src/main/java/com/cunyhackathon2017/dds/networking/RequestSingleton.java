package com.cunyhackathon2017.dds.networking;

/**
 * Created by Istolla on 4/29/2017.
 */
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Istolla on 11/28/2016.
 */
public class RequestSingleton {
    private static RequestSingleton instance = null;
    private static Context context = null;
    private RequestQueue requestQueue;

    private RequestSingleton(Context context) {
        RequestSingleton.context = context;
    }

    public static synchronized RequestSingleton getInstance(Context context) {
        if (instance == null)
            instance = new RequestSingleton(context);
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request) {
        getRequestQueue().add(request);
    }
}
