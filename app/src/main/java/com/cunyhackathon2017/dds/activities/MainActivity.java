package com.cunyhackathon2017.dds.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.cunyhackathon20017.dds.R;
import com.cunyhackathon2017.dds.fragments.JobFragment;
import com.cunyhackathon2017.dds.listeners.TeamsReceivedListener;
import com.cunyhackathon2017.dds.models.Team;
import com.cunyhackathon2017.dds.services.RESTfulCallService;
import com.cunyhackathon2017.dds.tools.ValuesHolder;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
                   JobFragment.TeamsRequestListener {
    NavigationView navigationView = null;
    FloatingActionButton fab = null;
    FragmentManager fragmentManager = null;
    Messenger serviceMessenger = null;
    boolean serviceIsBound = false;
    List<Message> pendingMessages = new ArrayList<>(); //Store messages that were not sent
    JobFragment jobFragment = null;
    TeamsReceivedListener teamsReceivedListener = null;
    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            serviceIsBound = true;
            serviceMessenger = new Messenger(service);
            Message registerMessage = Message.obtain(null, ValuesHolder.MSG_REGISTER_CLIENT);
            /*Create messenger that service will use to send replies.*/
            registerMessage.replyTo = new Messenger(new IncomingHandler());
            sendToService(registerMessage); //Send register message.
            /*Send any pending messages.*/
            if (!pendingMessages.isEmpty())
                for (int i = 0; i < pendingMessages.size(); i++)
                    sendToService(pendingMessages.remove(i));
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceIsBound = false;
            serviceConnection = null;
            Log.i(ValuesHolder.APP_NAME,
                    "[" + MainActivity.class.getName() + "]: service has " + "been disconnected");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fab = (FloatingActionButton) findViewById(R.id.id_fab);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        fragmentManager = getFragmentManager();
        fragmentManager
                .addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        if (fragmentManager.getBackStackEntryCount() == 0) {
                            navigationView.getMenu().getItem(0).setChecked(true);
                            fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                                    R.drawable.ic_group_work_white_48dp));
                        }
                        if (ValuesHolder.showFab)
                            fab.setVisibility(View.VISIBLE);
                        else
                            fab.setVisibility(View.INVISIBLE);
                    }
                });
        initJobFragment();
        showJobFragment();
    }

    @Override
    protected void onStart() {
        super.onStart();
        executeBindService();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (serviceIsBound)
            executeUnbindService();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.id_next_job) {
            showJobFragment();
            sendTeamsRequest();
        } else if (id == R.id.id_itenarary) {
        } else if (id == R.id.id_history) {
        } else if (id == R.id.id_settings) {
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onTeamsRequested() {
        sendTeamsRequest();
    }

    private void showJobFragment() {
        //TODO Display job fragment
        navigationView.getMenu().getItem(0).setChecked(true); //First item is checked by default
        showFragment(jobFragment, false);
    }

    private void showFragment(Fragment fragment, boolean addToBackStack) {
        if (fragment.getClass() != JobFragment.class)
            navigationView.getMenu().getItem(0).setChecked(false);
        if (addToBackStack)
            fragmentManager.beginTransaction().replace(R.id.content_main, fragment)
                           .addToBackStack(null).commit();
        else {
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); i++)
                fragmentManager.popBackStack();
            if (fragment != null)
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit();
        }
    }

    private void printErrorLog(String message) {
        Log.e(ValuesHolder.APP_NAME, "[" + RESTfulCallService.class.getName() + "]: " + message);
    }

    private void sendToService(Message message) {
        /*Send message to background service. If there is no connection between activity and
        service, store message to be sent when connection resumes.*/
        if (serviceIsBound) {
            try {
                serviceMessenger.send(message);
            } catch (RemoteException e) {
                printErrorLog(e.getMessage());
                e.printStackTrace();
            }
        } else
            pendingMessages.add(message);
    }

    private void sendTeamsRequest() {
        Message message = Message.obtain(null, ValuesHolder.MSG_REQUEST_TEAMS);
        sendToService(message);
    }

    private void executeBindService() {
        bindService(new Intent(this, RESTfulCallService.class), serviceConnection,
                Context.BIND_AUTO_CREATE);
    }

    private void executeUnbindService() {
        unbindService(serviceConnection);
        serviceIsBound = false;
    }

    private void initJobFragment() {
        jobFragment = JobFragment.newInstance();
        try {
            teamsReceivedListener = (TeamsReceivedListener) jobFragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    jobFragment.getClass() + " must implement " + TeamsReceivedListener.class
                            .getName());
        }
    }

    private class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch ((msg.what)) {
                default:
                    super.handleMessage(msg);
                    break;
                case ValuesHolder.MSG_TEAMS_RECEIVED: {
                    List<Team> teamList = msg.getData()
                                             .getParcelableArrayList(ValuesHolder.BUNDLE_TEAMS_KEY);
                    if (teamList != null)
                        Log.i(ValuesHolder.APP_NAME, "Team list received in main activity");
                    else
                        printErrorLog("Retrieved group list is null");
                    teamsReceivedListener.onTeamsReceived(teamList);
                }
                break;
            }
        }
    }
}
