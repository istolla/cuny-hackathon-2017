package com.cunyhackathon2017.dds.tools;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Calendar;

/**
 * Created by Istolla on 4/30/2017.
 */
public class DateDeserializer implements JsonDeserializer<Calendar> {
    @Override
    public Calendar deserialize(
            JsonElement dateString, Type type, JsonDeserializationContext context)
    throws JsonParseException {
        return DateConverter.toDate(dateString.getAsString());
    }
}
