package com.cunyhackathon2017.dds.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Istolla on 4/30/2017.
 */
public class DateConverter {
    private static String pattern = "yyyy-MM-dd HH:mm:ss";

    public static Calendar toDate(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Calendar cal = GregorianCalendar.getInstance();
        try {
            cal.setTime(simpleDateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }

    public static String fromDate(Calendar date) {
        return new SimpleDateFormat(pattern).format(date);
    }

    public static String getDate(Calendar date) {
        String datePattern = "yyyy-MM-dd";
        return new SimpleDateFormat(datePattern).format(date);
    }

    public static String getTime(Calendar date) {
        String timePattern = "HH:mm:ss";
        return new SimpleDateFormat(timePattern).format(date);
    }
}
