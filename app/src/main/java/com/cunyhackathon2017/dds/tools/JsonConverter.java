package com.cunyhackathon2017.dds.tools;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Calendar;

/**
 * Created by Istolla on 4/29/2017.
 */
public class JsonConverter {
    private static Gson gson = null;

    public static String toJson(Object object) {
        if (gson == null)
            init();
        return gson.toJson(object);
    }

    public static String toJson(Object object, Type objectType) {
        if (gson == null)
            init();
        return gson.toJson(object, objectType);
    }

    public static <T> T fromJson(String jsonString, TypeToken<?> typeToken) {
        if (gson == null)
            init();
        return gson.fromJson(jsonString, typeToken.getType());
    }

    public static <T> T fromJson(String jsonString, Type type) {
        if (gson == null)
            init();
        return gson.fromJson(jsonString, type);
    }

    private static void init() {
        gson = new GsonBuilder().setPrettyPrinting().serializeNulls()
                                .registerTypeAdapter(Calendar.class, new DateSerializer())
                                .registerTypeAdapter(Calendar.class, new DateDeserializer())
                                .enableComplexMapKeySerialization().create();
    }
}

