package com.cunyhackathon2017.dds.tools;

/**
 * Created by Istolla on 4/29/2017.
 */
public class ValuesHolder {
    public static final String APP_NAME = "DDS";
    public static final String RESTFUL_SERVICE_BASE_URL = "http://64.137.242.35/api/api/";
    public static final String JSON_MEDIA_TYPE = "application/json";
    public static final String PROTOCOL_CHARSET = "utf-8";
    public static final String DEVELOPER_EMAIL = "istolla.l.venorik@gmail.com";
    public static final String TAG_CREATE_DIALOG = "create dialog";
    public static int EMPLOYEE_ID = 47;
    public static boolean showFab = true;
    /*Bundle Keys*/
    public static final String BUNDLE_TEAMS_KEY = "teams";
    public static final String BUNDLE_EMPLOYEES_KEY = "employees";
    /*Messenger Values*/
    public static final int MSG_REGISTER_CLIENT = 1;
    public static final int MSG_REQUEST_TEAMS = 2;
    public static final int MSG_TEAMS_RECEIVED = 3;
}
