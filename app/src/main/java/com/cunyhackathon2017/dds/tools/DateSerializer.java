package com.cunyhackathon2017.dds.tools;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Calendar;

/**
 * Created by Istolla on 4/30/2017.
 */
public class DateSerializer implements JsonSerializer<Calendar> {
    @Override
    public JsonElement serialize(
            Calendar date, Type type, JsonSerializationContext context) {
        return new JsonPrimitive(DateConverter.fromDate(date));
    }
}
