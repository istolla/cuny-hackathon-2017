package com.cunyhackathon2017.dds.fragments;

import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cunyhackathon20017.dds.R;
import com.cunyhackathon2017.dds.listeners.TeamsReceivedListener;
import com.cunyhackathon2017.dds.models.Employee;
import com.cunyhackathon2017.dds.models.Job;
import com.cunyhackathon2017.dds.models.Team;
import com.cunyhackathon2017.dds.tools.DateConverter;
import com.cunyhackathon2017.dds.tools.ValuesHolder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Istolla on 4/30/2017.
 */
public class JobFragment extends Fragment implements TeamsReceivedListener {
    private static View rootView = null;
    private static FloatingActionButton fab = null;
    private static TeamsRequestListener teamsRequestListener = null;
    private static Job job = null;
    private static List<Employee> employees = null; //Teammates taking part in this job.
    private static TextView requesterView = null, startDateView = null, durationView = null,
            addressView = null, descriptionView;

    public JobFragment() {
    }

    public static JobFragment newInstance() {
        return new JobFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            teamsRequestListener = (TeamsRequestListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() + " must implement " + TeamsRequestListener.class.getName());
        }
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_job_detail, container, false);
        requesterView = (TextView) rootView
                .findViewById(R.id.id_fragment_job_detail_requester_name);
        startDateView = (TextView) rootView.findViewById(R.id.id_fragment_job_detail_request_date);
        durationView = (TextView) rootView.findViewById(R.id.id_fragment_job_detail_duration);
        addressView = (TextView) rootView.findViewById(R.id.id_fragment_job_detail_address);
        fab = (FloatingActionButton) getActivity().findViewById(R.id.id_fab);
        fab.setImageDrawable(ContextCompat
                .getDrawable(rootView.getContext(), R.drawable.ic_group_work_white_48dp));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTeamDialog();
            }
        });
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        ValuesHolder.showFab = true;
        teamsRequestListener.onTeamsRequested(); //Instruct main to request teams
    }

    @Override
    public void onTeamsReceived(List<Team> teams) {
        /*Parse through the team list for the next job.
        * Get teammates names.*/
        HashMap<Job, List<Employee>> teamHashMap = new HashMap<>();
        for (Team t : teams)
            teamHashMap.put(t.getJob(), t.getEmployees());
        /*Get next job*/
        ArrayList<Job> jobs = new ArrayList<>(teamHashMap.keySet());
        Collections.sort(jobs, new Comparator<Job>() {
            @Override
            public int compare(Job o1, Job o2) {
                int result = 0;
                if (o1.getStart() != null && o2.getStart() != null)
                    result = o1.getStart().compareTo(o2.getStart());
                return result;
            }
        });
        job = jobs.get(0); //This is the earliest job after Collections.sort()
        employees = teamHashMap.get(job); //Get the team members for this job.
        /*Set Views*/
        requesterView.setText(job.getRequesterName());
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(job.getStart());
        startDateView.setText(DateConverter.getDate(calendar));
        durationView.setText(job.getDuration());
        descriptionView.setText(job.getDescription());
    }

    private void showTeamDialog() {
        DialogFragment teamDialogFragment = new TeamDialogFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ValuesHolder.BUNDLE_EMPLOYEES_KEY,
                (ArrayList<? extends Parcelable>) employees);
        teamDialogFragment.setArguments(args);
        teamDialogFragment.show(getFragmentManager(), ValuesHolder.TAG_CREATE_DIALOG);
    }

    public interface TeamsRequestListener {
        void onTeamsRequested();
    }
}
