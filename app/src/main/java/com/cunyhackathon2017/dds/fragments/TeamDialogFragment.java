package com.cunyhackathon2017.dds.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.cunyhackathon20017.dds.R;
import com.cunyhackathon2017.dds.models.Employee;
import com.cunyhackathon2017.dds.tools.ValuesHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Istolla on 4/30/2017.
 */
public class TeamDialogFragment extends DialogFragment {
    List<Employee> employees = null;
    ListView employeeListView = null;
    ArrayAdapter<String> adapter = null;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        employees = getArguments().getParcelableArrayList(ValuesHolder.BUNDLE_EMPLOYEES_KEY);
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.dialog_fragment_employees, null);
        employeeListView = (ListView) view.findViewById(R.id.id_dialog_fragment_employees_list);
        List<String> nameList = new ArrayList<>();
        if (employees != null)
            for (Employee e : employees)
                nameList.add(e.getName());
        nameList.add("Agent Smith");
        nameList.add("Lex Luthor");
        nameList.add("Wilson Fisk");
        adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, nameList);
        employeeListView.setAdapter(adapter);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(view)
               .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {
                       getDialog().dismiss();
                   }
               });
        return builder.create();
    }
}
