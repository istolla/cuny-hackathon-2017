package com.cunyhackathon2017.dds.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cunyhackathon2017.dds.models.Team;
import com.cunyhackathon2017.dds.networking.GsonRequest;
import com.cunyhackathon2017.dds.networking.RequestSingleton;
import com.cunyhackathon2017.dds.tools.ValuesHolder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Istolla on 4/29/2017.
 */
public class RESTfulCallService extends Service {
    /*Create Messenger to receive client messages*/
    private final Messenger messenger = new Messenger(new IncomingHandler());
    /*Create clientMessenger to send messages to client*/
    private Messenger clientMessenger = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }

    private void sendToClient(Message message) {
        if (clientMessenger != null) {
            try {
                clientMessenger.send(message);
            } catch (RemoteException e) {
                printErrorLog(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private void printErrorLog(String message) {
        Log.e(ValuesHolder.APP_NAME, "[" + RESTfulCallService.class.getName() + "]: " + message);
    }

    private String replaceSpace(String string) {
        return string.replace(" ", "%20");
    }

    private class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                default:
                    super.handleMessage(msg);
                    break;
                case ValuesHolder.MSG_REGISTER_CLIENT: {
                    clientMessenger = msg.replyTo;
                    Log.i(ValuesHolder.APP_NAME,
                            "[" + RESTfulCallService.class.getName() + "]: registered.");
                }
                break;
                case ValuesHolder.MSG_REQUEST_TEAMS: {
                    Type listType = new TypeToken<List<Team>>() {
                    }.getType();
                    String url = ValuesHolder.RESTFUL_SERVICE_BASE_URL + "Teams/"
                                 + ValuesHolder.EMPLOYEE_ID;
                    GsonRequest<List<Team>> gsonGetRequest = new GsonRequest<>(url, listType, null,
                            new Response.Listener<List<Team>>() {
                                @Override
                                public void onResponse(List<Team> response) {
                                    Bundle bundle = new Bundle();
                                    bundle.putParcelableArrayList(ValuesHolder.BUNDLE_TEAMS_KEY,
                                            (ArrayList<? extends Parcelable>) response);
                                    Message reply = Message
                                            .obtain(null, ValuesHolder.MSG_TEAMS_RECEIVED);
                                    reply.setData(bundle);
                                    sendToClient(reply);
                                    Log.i(ValuesHolder.APP_NAME,
                                            "Team list received in REST activity.");
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            printErrorLog(error.getMessage());
                        }
                    });
                    RequestSingleton.getInstance(getApplicationContext())
                                    .addToRequestQueue(gsonGetRequest);
                }
            }
        }
    }
}
