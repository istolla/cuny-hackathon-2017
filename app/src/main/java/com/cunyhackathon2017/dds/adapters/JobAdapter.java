package com.cunyhackathon2017.dds.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cunyhackathon2017.dds.models.Team;

import java.util.List;

/**
 * Created by Istolla on 4/30/2017.
 */
public class JobAdapter extends RecyclerView.Adapter<JobAdapter.ViewHolder> {
    List<Team> teamList;
    public JobAdapter(List<Team> teamList) {
        super();
        this.teamList = teamList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        String requesterName, date, time;
        TextView requesterTextView, dateTextView, timeTextView;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
