package com.cunyhackathon2017.dds.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Istolla on 4/29/2017.
 */
public class Competence implements Parcelable{
    private int id;
    private String name;
    private String competenceType;
    private ArrayList<CompetenceLevel> competenceLevels;

    private static Creator<Competence> CREATOR = new Creator<Competence>() {
        @Override
        public Competence createFromParcel(Parcel source) {
            return new Competence(source);
        }

        @Override
        public Competence[] newArray(int size) {
            return new Competence[size];
        }
    };

    private Competence(Parcel source) {
        id = source.readInt();
        name = source.readString();
        competenceType = source.readString();
        competenceLevels = source.readArrayList(CompetenceLevel.class.getClassLoader());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompetenceType() {
        return competenceType;
    }

    public void setCompetenceType(String competenceType) {
        this.competenceType = competenceType;
    }

    public ArrayList<CompetenceLevel> getCompetenceLevels() {
        return competenceLevels;
    }

    public void setCompetenceLevels(
            ArrayList<CompetenceLevel> competenceLevels) {
        this.competenceLevels = competenceLevels;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(competenceType);
        dest.writeList(competenceLevels);
    }
}
