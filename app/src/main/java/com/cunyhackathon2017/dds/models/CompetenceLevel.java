package com.cunyhackathon2017.dds.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Istolla on 4/29/2017.
 */
public class CompetenceLevel implements Parcelable {
    private int id;
    private Competence competence;
    private Level level;
    private static Creator<CompetenceLevel> CREATOR = new Creator<CompetenceLevel>() {
        @Override
        public CompetenceLevel createFromParcel(Parcel source) {
            return new CompetenceLevel(source);
        }

        @Override
        public CompetenceLevel[] newArray(int size) {
            return new CompetenceLevel[size];
        }
    };

    private CompetenceLevel(Parcel source) {
        id = source.readInt();
        competence = source.readParcelable(Competence.class.getClassLoader());
        level = source.readParcelable(Level.class.getClassLoader());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Competence getCompetence() {
        return competence;
    }

    public void setCompetence(Competence competence) {
        this.competence = competence;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
       dest.writeParcelable(competence, flags);
        dest.writeParcelable(level, flags);
    }
}
