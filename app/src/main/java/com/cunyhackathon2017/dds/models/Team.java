package com.cunyhackathon2017.dds.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Istolla on 4/29/2017.
 */
public class Team implements Parcelable{
    private static int id;
    private ArrayList<Employee> employees;
    private Job job;
    private static Creator<Team> CREATOR = new Creator<Team>() {
        @Override
        public Team createFromParcel(Parcel source) {
            return new Team(source);
        }

        @Override
        public Team[] newArray(int size) {
            return new Team[size];
        }
    };

    private Team(Parcel source){
        id = source.readInt();
        employees = source.readArrayList(Employee.class.getClassLoader());
        job = source.readParcelable(Job.class.getClassLoader());
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Team.id = id;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(ArrayList<Employee> employees) {
        this.employees = employees;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeList(employees);
        dest.writeParcelable(job, flags);
    }
}
