package com.cunyhackathon2017.dds.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Istolla on 4/29/2017.
 */
public class Level implements Parcelable {
    private int id;
    private int value;
    private ArrayList<CompetenceLevel> competenceLevels;
    private static Creator<Level> CREATOR = new Creator<Level>() {
        @Override
        public Level createFromParcel(Parcel source) {
            return new Level(source);
        }

        @Override
        public Level[] newArray(int size) {
            return new Level[size];
        }
    };

    private Level(Parcel source) {
        id = source.readInt();
        value = source.readInt();
        competenceLevels = source.readArrayList(CompetenceLevel.class.getClassLoader());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public ArrayList<CompetenceLevel> getCompetenceLevels() {
        return competenceLevels;
    }

    public void setCompetenceLevels(
            ArrayList<CompetenceLevel> competenceLevels) {
        this.competenceLevels = competenceLevels;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(value);
        dest.writeList(competenceLevels);
    }
}
