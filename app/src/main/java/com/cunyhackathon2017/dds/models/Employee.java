package com.cunyhackathon2017.dds.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Istolla on 4/29/2017.
 */
public class Employee implements Parcelable {
    private int id;
    private String name;
    private Office office;
    private ArrayList<CompetenceLevel> competenceLevels;
    private static Creator<Employee> CREATOR = new Creator<Employee>() {
        @Override
        public Employee createFromParcel(Parcel source) {
            return new Employee(source);
        }

        @Override
        public Employee[] newArray(int size) {
            return new Employee[size];
        }
    };

    private Employee(Parcel source) {
        id = source.readInt();
        name = source.readString();
        office = source.readParcelable(Office.class.getClassLoader());
        competenceLevels = source.readArrayList(CompetenceLevel.class.getClassLoader());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public ArrayList<CompetenceLevel> getCompetenceLevels() {
        return competenceLevels;
    }

    public void setCompetenceLevels(
            ArrayList<CompetenceLevel> competenceLevels) {
        this.competenceLevels = competenceLevels;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeParcelable(office, flags);
        dest.writeList(competenceLevels);
    }
}
