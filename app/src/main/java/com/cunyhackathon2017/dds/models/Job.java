package com.cunyhackathon2017.dds.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Istolla on 4/29/2017.
 */
public class Job implements Parcelable {
    private int id;
    private ArrayList<CompetenceLevel> competenceLevels;
    int duration;
    private Date start; //Start date
    private String latitude, longitude;
    private String requesterName;
    private String addressLine1, addressLine2;
    private String city;
    private String state;
    private String zip;
    private String description;
    private static Creator<Job> CREATOR = new Creator<Job>() {
        @Override
        public Job createFromParcel(Parcel source) {
            return new Job(source);
        }

        @Override
        public Job[] newArray(int size) {
            return new Job[size];
        }
    };

    private Job(Parcel source) {
        id = source.readInt();
        competenceLevels = source.readArrayList(CompetenceLevel.class.getClassLoader());
        duration = source.readInt();
        start = new Date(source.readInt());
        latitude = source.readString();
        longitude = source.readString();
        requesterName = source.readString();
        addressLine1 = source.readString();
        addressLine2 = source.readString();
        city = source.readString();
        state = source.readString();
        zip = source.readString();
        description = source.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<CompetenceLevel> getCompetenceLevels() {
        return competenceLevels;
    }

    public void setCompetenceLevels(
            ArrayList<CompetenceLevel> competenceLevels) {
        this.competenceLevels = competenceLevels;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getRequesterName() {
        return requesterName;
    }

    public void setRequesterName(String requesterName) {
        this.requesterName = requesterName;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeList(competenceLevels);
        dest.writeInt(duration);
        dest.writeLong(start.getTime());
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(requesterName);
        dest.writeString(addressLine1);
        dest.writeString(addressLine2);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(zip);
        dest.writeString(description);
    }
}
