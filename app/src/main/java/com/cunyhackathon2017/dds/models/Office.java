package com.cunyhackathon2017.dds.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Istolla on 4/29/2017.
 */
public class Office implements Parcelable {
    private int id;
    private String name;
    private String latitude;
    private String longitude;
    private ArrayList<Employee> employees;
    private Enterprise enterprise;
    private static Creator<Office> CREATOR = new Creator<Office>() {
        @Override
        public Office createFromParcel(Parcel source) {
            return new Office(source);
        }

        @Override
        public Office[] newArray(int size) {
            return new Office[size];
        }
    };

    private Office(Parcel source) {
        id = source.readInt();
        latitude = source.readString();
        longitude = source.readString();
        employees = source.readArrayList(Employee.class.getClassLoader());
        enterprise = source.readParcelable(Enterprise.class.getClassLoader());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(ArrayList<Employee> employees) {
        this.employees = employees;
    }

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeList(employees);
        dest.writeParcelable(enterprise, flags);
    }
}
